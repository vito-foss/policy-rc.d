#!/usr/bin/env python3

import argparse
import sys


class ArgumentParser(argparse.ArgumentParser):
    def __init__(self, *args, quiet=False, **kwargs):
        super().__init__(*args, **kwargs)
        self.quiet = quiet

    def exit(self, status=0, message=None):
        if message and not self.quiet:
            sys.stderr.write(message)
        sys.exit(status)

    def error(self, message):
        if not self.quiet:
            self.print_usage(sys.stderr)
        self.exit(102, f"{self.prog}: error: {message}\n")


def parse_args():
    default_parser = ArgumentParser(add_help=False)
    default_parser.add_argument("--list", action="store_true")
    default_parser.add_argument("--quiet", action="store_true")

    args, _ = default_parser.parse_known_args()

    usage = "\n       ".join((
        "%(prog)s [options] <initscript ID> <actions> [<runlevel>]",
        "%(prog)s [options] --list <initscript ID> [<runlevel> ...]"
    ))
    parser = ArgumentParser(
        prog="/usr/sbin/policy-rc.d",
        parents=[default_parser],
        usage=usage,
        quiet=args.quiet,
    )
    if args.list:
        parser.add_argument("service", metavar="<initscript ID>", type=str, nargs=1)
        parser.add_argument("runlevel", metavar="<runlevel>", type=int, nargs="*")
    else:
        parser.add_argument("service", metavar="<initscript ID>", type=str, nargs=1)
        parser.add_argument("actions", metavar="<actions>", type=str, nargs=1)
        parser.add_argument("runlevel", metavar="<runlevel>", type=int, nargs="?")
    return parser.parse_args()


def main():
    args = parse_args()

    print(args)


if __name__ == "__main__":
    main()
